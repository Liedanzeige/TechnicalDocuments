﻿# Interface des CommandServers

Der DisplayServer läuft auf Port 8080 und ist ein HTTPS-Server.<br>
Strings in den Antworten sind unicode kodiert.

 todo authentification

## Abfragen
### Abfrage der Version
```
HTTP GET /version.json
```
Gibt die Version des MediaDisplay-Servers zurück. Nicht gleich mit der Version der Schnittstelle.
###### Parameter
keine
###### Beispiel
```
HTTP GET version.json
{
	"ok" : true,
	"version": "0.5.0",
	"build": "00006",
	"commit": "f247a697f185a56df8b243fbe9ef672947960b42"
}
```

### Lied soll angezeigt werden
```
HTTP GET /displaySong.json
```
###### Parameter
Parameter | Beschreibung | Bemerkung
--------- | ------------ | ---------
book | Abkürzung des Liederbuches | Erforderlich
number | Nummer des Liedes | Erforderlich
title | Titel des Liedes | Optional, default: ""
sopran | 1 wenn die Solostimme angezeigt werden soll, sonst 0 | Optional, default: 0
alt | 1 wenn die Solostimme angezeigt werden soll, sonst 0 | Optional, default: 0
tenor | 1 wenn die Solostimme angezeigt werden soll, sonst 0 | Optional, default: 0
bass | 1 wenn die Solostimme angezeigt werden soll, sonst 0 | Optional, default: 0
sopranname | Name des Sopransängers | Optional, default: "Sopran"
altname | Name des Altsängers | Optional, default: "Alt"
tenorname | Name des Tenorsängers | Optional, default: "Tenor"
bassname | Name der Bassängers | Optional, default: "Bass"
token | Sessiontoken | Erforderlich
###### Fehlermeldungen
 errnr | error | Bedeutung
 ----- | ----- | ---------
 2 | Change state error. | Liedanzeige kann den Zustand in den gewünschten Zustand nicht wechseln.
###### Beispiel
```
HTTP GET displaySong.json?book=ChLI&number=123a&title=Ein%20guter%20Tag&sopran=1&alt=1&token=123456789abcdefghijklmnopqrstuvwxyz
{
	"ok" : true
}
```

### Letztes Lied nochmal anzeigen
```
HTTP GET /displayLastSong.json
```
###### Parameter
Parameter | Beschreibung | Bemerkung
--------- | ------------ | ---------
token | Sessiontoken | Erforderlich
###### Beispiel
```
HTTP GET displayLastSong.json?token=123456789abcdefghijklmnopqrstuvwxyz
{
	"ok" : true
}
```

### Uhr anzeigen
```
HTTP GET /displayClock.json
```
###### Parameter
Parameter | Beschreibung | Bemerkung
--------- | ------------ | ---------
token | Sessiontoken | Erforderlich
###### Beispiel
```
HTTP GET displayClock.json?token=123456789abcdefghijklmnopqrstuvwxyz
{
	"ok" : true
}
```

### Nachricht als Laufschrift anzeigen
```
HTTP GET /displayMessage.json
```
###### Parameter
Parameter | Beschreibung | Bemerkung
--------- | ------------ | ---------
message | Nachricht, die angezeigt werden soll | Erforderlich
token | Sessiontoken | Erforderlich
###### Beispiel
```
HTTP GET displayMessage.json?message=Hier könnte Ihre Werbung stehen&token=123456789abcdefghijklmnopqrstuvwxyz
{
	"ok" : true
}
```
### Wetterdaten abfrage
```
HTTP GET /weather.json
```
Temperatur in °C, Druck in hPa, relative Luftfeuchtigkeit in %
###### Beispiel
```
HTTP GET /weather.json
{
	"temp" : 23.4,
	"pressure" : 1013.25,
	"humidity" : 34.0,
	"date" : "2017-05-31 00:07:50.083000"
}
```
###### Beispiel für ungültige Werte
```
HTTP GET /weather.json
{
	"temp" : -100.0,
	"pressure" : -100.0,
	"humidity" : -100.0,
	"date" : "2017-05-31 00:07:50.083000"
}
```

### Liedanzeige herunterfahren
```
HTTP GET /shutdown.json
```
###### Parameter
Parameter | Beschreibung | Bemerkung
--------- | ------------ | ---------
token | Sessiontoken | Erforderlich
###### Beispiel
```
HTTP GET shutdown.json?token=123456789abcdefghijklmnopqrstuvwxyz
{
	"ok" : true
}
```

### Liedanzeige in Standby versetzen
```
HTTP GET /standby.json
```
###### Parameter
Parameter | Beschreibung | Bemerkung
--------- | ------------ | ---------
token | Sessiontoken | Erforderlich
###### Beispiel
```
HTTP GET standby.json?token=123456789abcdefghijklmnopqrstuvwxyz
{
	"ok" : true
}
```

### Liedanzeige Standby beenden
```
HTTP GET /wakeup.json
```
###### Parameter
Parameter | Beschreibung | Bemerkung
--------- | ------------ | ---------
token | Sessiontoken | Erforderlich
###### Beispiel
```
HTTP GET wakeup.json?token=123456789abcdefghijklmnopqrstuvwxyz
{
	"ok" : true
}
```

### Liedanzeige neu starten
```
HTTP GET /reboot.json
```
###### Parameter
Parameter | Beschreibung | Bemerkung
--------- | ------------ | ---------
token | Sessiontoken | Erforderlich
###### Beispiel
```
HTTP GET reboot.json?token=123456789abcdefghijklmnopqrstuvwxyz
{
	"ok" : true
}
```

## Fehlermeldungen
 errnr | error | Bedeutung
 ----- | ----- | ---------
 0 | No such request. | Es wurde eine unbekannte Anfrage geschickt.
 1 | Authentication failed. | Benutername und/oder Passwort sind/ist falsch
 2 | Change state error. | Liedanzeige kann den Zustand in den gewünschten Zustand nicht wechseln.
 
###### Beispiel
```
HTTP GET /getSomethingFaulty
{
	"ok" : false,
	"errno": 0,
	"error": "No such request."
}
```
