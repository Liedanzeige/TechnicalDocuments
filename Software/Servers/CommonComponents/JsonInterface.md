﻿Rückgabewert ist immer ein [JSON-Objekt](https://de.wikipedia.org/wiki/JavaScript_Object_Notation). Es besitzt immer den Key "ok", der angibt ob der Befehl erfolgreich ausgeführt wurde oder nicht. Wenn er true ist ging alles gut, wenn er false ist gibt es eine texutelle Fehlermeldung mit dem Key "error" und eine Fehlernummer mit dem Key "errnr". Eine Liste aller Fehlermeldungen ist am Ende des Dokuments

## Befehle
### System
#### login
```
HTTP GET /system/login
```
###### Parameter
 
 Parameter | Beschreibung | Bemerkung
 --------- | ------------ | ---------
 name | Benutzername | Erforderlich
 password | Passwort | Erforderlich
###### Fehlermeldungen
 errnr | error | Bedeutung
 ----- | ----- | ---------
 0 | Invalid name or password. | Benutername und/oder Passwort sind/ist falsch
 1 | User already logged in. | Benutzer ist schon eingeloggt.
###### Beispiel
```
HTTP GET /system/login?name=admin&password=admin
{
	"ok" : true,
	"token" : "123456789abcdefghijklmnopqrstuvwxyz"
}
```
#### logout
```
HTTP GET /system/logout
```
###### Parameter
Parameter | Beschreibung | Bemerkung
--------- | ------------ | ---------
 token | Sessiontoken | Erforderlich
###### Beispiel
```
HTTP GET /system/logout?token=123456789abcdefghijklmnopqrstuvwxyz
{
	"ok" : true
}
```
#### getVersion
todo
#### getDiagnostics
todo
### Anzeige
#### setSong
```
HTTP GET /screen/setSong
```
###### Parameter
Parameter | Beschreibung | Bemerkung
--------- | ------------ | ---------
book | Abkürzung des Liederbuches | Erforderlich
number | Nummer des Liedes | Erforderlich
sopran | 1 wenn Solo Sopran, sonst 0 | Optional, default: 0
alt | 1 wenn Solo Alt, sonst 0 | Optional, default: 0
tenor | 1 wenn Solo Tenor, sonst 0 | Optional, default: 0
bass | 1 wenn Solo Bass, sonst 0 | Optional, default: 0
token | Sessiontoken | Erforderlich
###### Fehlermeldungen
 errnr | error | Bedeutung
 ----- | ----- | ---------
 4 | Book not found. | Das angegebene Liederbuch konnte nicht gefunden werden.
 5 | Song not found. | Das angegebene Lied konnte nicht im angegebenen Liederbuch gefunden werden.
###### Beispiel
```
HTTP GET /screen/setSong?book=ChLI&number=123&sopran=1&alt=1&token=123456789abcdefghijklmnopqrstuvwxyz
{
	"ok" : true
}
```
#### setClock
```
HTTP GET /screen/setClock
```
###### Parameter
Parameter | Beschreibung | Bemerkung
--------- | ------------ | ---------
token | Sessiontoken | Erforderlich
###### Beispiel
```
HTTP GET /screen/setClock&token=123456789abcdefghijklmnopqrstuvwxyz
{
	"ok" : true
}
```
#### setStop
todo  ist dazu gedacht den Prediger anzuhalten
#### setRecording
todo ist für Aufnahmen gedacht
#### getSong
Gibt das zuletzt angezeigte Lied zurück, auch wenn es nicht mehr angezeigt wird. Dieses Feature ist zur Integration in MediaNames gedacht. Es ist nicht über ein Token abgesichert.
```
HTTP GET /screen/getSong
```
###### Parameter
Keine Parameter
###### Fehlermeldungen
 errnr | error | Bedeutung
 ----- | ----- | ---------
 6 | No Song. | Es wurde noch kein Lied angegeben.
###### Beispiel
```
HTTP GET /screen/getSong
{
	"ok" : true,
	"title": "Jesus ruft freundlich"
}
```

## Fehlermeldungen
 errnr | error | Bedeutung
 ----- | ----- | ---------
 0 | Invalid name or password. | Benutername und/oder Passwort sind/ist falsch
 1 | User already logged in. | Benutzer ist schon eingeloggt.
 2 | Not logged in. | Benutzer ist nicht eingeloggt, die Sitzung ist abgelaufen oder hat nicht die erforderlichen Rechte für diese Aktion.
 3 | No such command. | Es wurde ein unbekannter Befehl geschickt.
 4 | Book not found. | Das angegebene Liederbuch konnte nicht gefunden werden.
 5 | Song not found. | Das angegebene Lied konnte nicht im angegebenen Liederbuch gefunden werden.
 6 | No song. | Es wurde noch kein Lied angegeben.
 
###### Beispiel
```
HTTP GET /system/getSomethingFaulty
{
	"ok" : false,
	"errno": 3,
	"error": "No such command."
}
```
