﻿# Interface des DisplayServers

Der DisplayServer läuft auf Port 48080

## Abfragen
### Webseite für den Tontechniker
```
HTTP GET /index.html
```

### Webseite für gesungene Lieder
```
HTTP GET /songs.html
```

### Webseite für Wetterdaten
```
HTTP GET /wetter.html
```

### Webseite für allgemeine Lieder
```
HTTP GET /slave.html
```

### Webseite für Lizenzen
```
HTTP GET /lizenzen.html
```

### Über
```
HTTP GET /about.html
```

### Sicherheitsabfrage, ob es sich um eine Liedanzeige handelt
Wird von MediaTools verwendet.
```
HTTP GET /erstetelefonnachricht
```
###### Beispiel
```
HTTP GET /erstetelefonnachricht
daspferdfrisstkeinengurkensalat
```
### Favicon
```
HTTP GET /favicon.ico
```
### Kurzanleitung
```
HTTP GET /Kurzanleitung.jpg
```
### Abfrage für den Slave
```
HTTP GET /slave.json
```
###### Beispiel, wenn kein allgemeines Lied angezeigt wird
```
HTTP GET /slave.json
{
	"state" : "idle",
	"data" : null
}
```
###### Beispiel, wenn ein allgemeines Lied angezeit wird und es nur ein allgemeines Liederbuch gibt
```
HTTP GET /slave.json
{
	"state" : "song",
	"data" : { 
				"title": "Ein Lied",
				"number": "123"
			 }
}
```
###### Beispiel, wenn ein allgemeines Lied angezeit wird und es mehrere allgemeine Liederbücher gibt
```
HTTP GET /slave.json
{
	"state" : "song",
	"data" : { 
				"book": "5",
				"title": "Ein Lied",
				"color": "ffff00",
				"number": "123"
			 }
}
```
###### Beispiel, wenn die Liedanzeige im Standby ist
```
HTTP GET /slave.json
{
	"state" : "standby",
	"data" : null
}
```
###### Beispiel, wenn die Liedanzeige heruntergefahren wird
```
HTTP GET /slave.json
{
	"state" : "shutdown",
	"data" : null
}
```
### Abfrage des Datums und der Uhrzeit
```
HTTP GET /datetime.json
```
Datum und Uhrzeit im ISO-Format
###### Beispiel
```
HTTP GET /datetime.json
{
	"datetime" : "2018-07-01T21:26:37.298000"
}
```
### Abfrage der Wetterdaten
```
HTTP GET /weather.json
```
Temperatur in °C, Druck in hPa, relative Luftfeuchtigkeit in %
###### Beispiel
```
HTTP GET /weather.json
{
	"temp" : 23.4,
	"pressure" : 1013.25,
	"humidity" : 34.0,
	"date" : "2017-05-31 00:07:50.083000"
}
```
###### Beispiel für ungültige Werte
```
HTTP GET /weather.json
{
	"temp" : -100.0,
	"pressure" : -100.0,
	"humidity" : -100.0,
	"date" : "2017-05-31 00:07:50.083000"
}
```
### Abfrage des Status
```
HTTP GET /state.json
```
###### Beispiel wenn Liedanzeige in Grundzustand
```
HTTP GET /state.json
{
	"state": "Zeige Uhr", 
	"data": null, 
	"state_number": 1
}
```
###### Beispiel, wenn Status == "Zeige Lied"
```
HTTP GET /state.json
{
	"state": "Zeige Lied", 
	"data": {
				"general_song": false, 
				"book_color": "#ffff00", 
				"bass": "0", 
				"sopran_name": "Sopran", 
				"title": "", 
				"tenor_name": "Tenor", 
				"tenor": "0", 
				"bass_name": "Bass",
				"number": "12", 
				"sopran": "0", 
				"alt_name": "Alt", 
				"book_id": "1", 
				"book_abbreviation": "1", 
				"date": "2018-02-18T13:43:17.817000", 
				"alt": "0"
			}, 
	"state_number": 2
}
```
### Abfrage der gesungenen Lieder
```
HTTP GET /sungsongs.json
```
###### Beispiel
```
HTTP GET /sungsongs.json
[
	{
		"general_song": false, 
		"book_color": "#ffff00", 
		"bass": "0", 
		"sopran_name": "Sopran", 
		"title": "", 
		"tenor_name": "Tenor", 
		"tenor": "0", 
		"bass_name": "Bass",
		"number": "12", 
		"sopran": "0", 
		"alt_name": "Alt", 
		"book_id": "1", 
		"book_abbreviation": "1", 
		"date": "2018-02-18T13:43:17.817000", 
		"alt": "0"
	}, 
	{
		"general_song": false, 
		"book_color": "#ffff00", 
		"bass": "0", 
		"sopran_name": "Sopran", 
		"title": "", 
		"tenor_name": "Tenor", 
		"tenor": "1", 
		"bass_name": "Bass", 
		"number": "56", 
		"sopran": "0", 
		"alt_name": "Alt", 
		"book_id": "2", 
		"book_abbreviation": "2", 
		"date": "2018-02-18T13:43:17.817000", 
		"alt": "0"
	}
]
```
### Abfrage der Version des DisplayServer
```
HTTP GET /version.json
```
###### Beispiel
```
HTTP GET /version.json
"0.5"
```
### Abfrage der Version der Application (MediaDisplay-Server)
```
HTTP GET /applicationversion.json
```
###### Beispiel
```
HTTP GET /applicationversion.json
"1.1.0"
```
### Abfrage der Buildnummer der Application (MediaDisplay-Server)
```
HTTP GET /applicationbuildnumber.json
```
###### Beispiel
```
HTTP GET /applicationbuildnumber.json
"00008"
```
